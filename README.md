RUN PROGRAM:

`sbt "run-main com.here.DigraphApp /path/to/the/file"`

you can also run:

`sbt "run-main com.here.DigraphApp"` it will load example data and will print program results.

RUN TESTS:

`sbt test`

ASSUMPTIONS:

- has no loops (A -> A or B -> B etc.)

- has no edges with negative weight

- fully connected / has no unconnected cluster

- input file is correct and has the same structure as an example (didn't make a lot of validations on input data etc.)

- input file is not `huge` (it is possible to load data in memory), but it is possible to update implementation with incremental reading of data

- distances are Integers for current (but I designed API to be able process other type of data as well)

DESIGN DECISIONS:

- graph structure:

 * I made it possible to use any type of data as vertices(in an example it is String, but you can also use custom data structure)
  in order to make solution more generic, and to use indexes of this data instead of this objects for the algorithm.
  And any data structure for weights as soon you will be able to provide Monoid for operations on them and Ordering as well.
  
  For primitive type you just need to add implicit with standard implementation and everything will work:
  
      `implicit val tentativeMonoidInt = new TentativeMonoid[Int]`
      
       `implicit val tentativeOrderingInt = new TentativeOrdering[Int]`
       
 * I indexed Vertices in HashMap in order to save some memory and make algorithm more efficient,
 so we will use it later to decose indices back to Vertices objects.
 
 * Instead of making full matrix of all connections (N*N, where N - amount of vertices)
 
 I decided to use Array[Array[]] (N * M[-],  where N - amount of vertices, M[K] - amount of neighbours of K vertice) of such structure:
 
    0 -> [0, 2]
    
    1 -> [1, 3]
    
    2 -> []
    
    3 -> [2, 1, 0]
    
    etc.
    
 It is added more complexity to implementation, but I believe has it's own advantages.
 So, for most of operation for this implementation algorithm I need to have effective way to access and to update data,
 so there are two good alternatives: Array vs Vector.
 http://docs.scala-lang.org/overviews/collections/performance-characteristics.html
 
 I could use Vectors as more safe data structure,
 but Array in combination with primitive types uses much less memory (on the big graph - it makes sense).
 (p.s.: you can read some reasoning in comments of com.here.graph.Digraph)
 Also: http://www.lihaoyi.com/post/BenchmarkingScalaCollections.html#arrays-are-great
 I used Vector for building graph from input data because it has nicer append and prepend operations,
 and we don't know how many vertices we will have from the very beginning.


 FUTURE IMPROVEMENTS:
 
 - Data Structures that I selected a pretty arguable,
 but you really need to understand domain of application this algorithm and what data you will operate.
 
 - There are a lot of implementations for Dijkstra algorithm that can make solutions more effective,
 and highly build on the decision about data structures to use.
 
e.g. The implementation based on a min-priority queue implemented by a Fibonacci heap and running in O(|E|+|V|logV)
(where |E| is the number of edges). It is possible to create a data structure which has the same worst-case performance
as the Fibonacci heap has amortized performance. One such structure, the Brodal queue, is, in the words of the creator,
"quite complicated" and "[not] applicable in practice." Created in 2012, the strict Fibonacci heap[5] is a simpler
(compared to Brodal's) structure with the same worst-case bounds.

It is unknown whether the strict Fibonacci heap is efficient in practice.
The run-relaxed heaps of Driscoll et al. give good worst-case performance for all Fibonacci heap operations except merge.

- Amortized structures perform poorly in parallel environments. The relaxed heaps improves the performance of Dijkstra’s
shortest-path algorithm in parallel setting.

- Also it is possible to do some caching for finding routes and nearby, we can save intermediate results
(e/g/ for routes we can check if in previous time two vertices where visited we can use results of previous computations for building shortest route)


