package com.here.algorithm

import com.here.graph._
import cats.implicits._
import com.here.algorithm.BreadthFirstSearch._
import org.scalatest.{Matchers, WordSpec}

class BreadthFirstSearchTest extends WordSpec with Matchers {

  implicit val tentativeMonoidInt = new TentativeMonoid[Int]
  implicit val tentativeOrderingInt = new TentativeOrdering[Int]

  val exampleEdges = Seq(
    RouteEdge("A", "B", 240),
    RouteEdge("A", "C", 70),
    RouteEdge("A", "D", 120),
    RouteEdge("C", "B", 60),
    RouteEdge("D", "E", 480),
    RouteEdge("C", "E", 240),
    RouteEdge("B", "E", 210),
    RouteEdge("E", "A", 300),
    RouteEdge("E", "F", 100))




  "BFS" when {
    "destionation path" should {
      "work for small unweighted graph" in {
        BreadthFirstSearchImpl(Digraph.fromEdges(Seq(RouteEdge("A", "B", 1))))
          .route("A", "B").right.map(_._1) shouldBe Right(Seq(SimplePath("A", "B")))
      }

      "work for small unweighted graph A -> B -> C" in {
        val query = BreadthFirstSearchImpl(Digraph.fromEdges(Seq(
          RouteEdge("A", "B", 1),
          RouteEdge("B", "C", 1)
        )))
        query.route("A", "B").right.map(_._1) shouldBe Right(Seq(
          SimplePath("A", "B")
        ))
        query.route("A", "C").right.map(_._1) shouldBe Right(Seq(
          SimplePath("A", "B"),
          SimplePath("B", "C")
        ))
      }


      "work for small weighted graph A -3-> C, A -1-> B, B -1-> C" in {
        BreadthFirstSearchImpl(Digraph.fromEdges(Seq(
          RouteEdge("A", "C", 3),
          RouteEdge("A", "B", 1),
          RouteEdge("B", "C", 1)
        ))).route("A", "C").right.map(_._1) shouldBe Right(Seq(
          SimplePath("A", "B"),
          SimplePath("B", "C")
        ))
      }

      "work for example from task" in {
        val path = BreadthFirstSearchImpl(Digraph.fromEdges(exampleEdges))
          .route("A", "B")
        path.map(_._1) shouldBe Right(Seq(
          SimplePath("A", "C"),
          SimplePath("C", "B")
        ))
        path.map(_._2) shouldBe Right(130)
      }

      val edgesNumbersN = Seq(
        RouteEdge("4", "2", 1),
        RouteEdge("2", "3", 1),
        RouteEdge("3", "2", 1),
        RouteEdge("6", "0", 1),
        RouteEdge("0", "1", 1),
        RouteEdge("2", "0", 1),
        RouteEdge("11", "12", 1),
        RouteEdge("12", "9", 1),
        RouteEdge("9", "10", 1),
        RouteEdge("9", "11", 1),
        RouteEdge("8", "9", 1),
        RouteEdge("10", "12", 1),
        RouteEdge("11", "4", 1),
        RouteEdge("4", "3", 1),
        RouteEdge("3", "5", 1),
        RouteEdge("6", "8", 1),
        RouteEdge("8", "6", 1),
        RouteEdge("5", "4", 1),
        RouteEdge("0", "5", 1),
        RouteEdge("6", "4", 1),
        RouteEdge("6", "9", 1),
        RouteEdge("7", "5", 1)
      )

      "work for unweighted graph and not existent route" in {
        val query = BreadthFirstSearchImpl(Digraph.fromEdges(edgesNumbersN))
        query.route("1", "0") shouldBe  Left(DestinationIsNotReachableError)
        query.route("18", "0") shouldBe  Left(VerticeIsNotInGraphError)
        query.route("1", "jjjj") shouldBe  Left(VerticeIsNotInGraphError)
      }
    }


    "all paths" should {
      "work for small unweighted graph" in {
        BreadthFirstSearchImpl(Digraph.fromEdges(Seq(RouteEdge("A", "B", 1)))).nearby("A") shouldBe Right(Seq(Path("A", "B", 1)))
      }

      "work for small unweighted graph A -> B -> C" in {
        BreadthFirstSearchImpl(Digraph.fromEdges(Seq(
          RouteEdge("A", "B", 1),
          RouteEdge("B", "C", 1)
        ))).nearby("A") shouldBe Right(Seq(
          Path("A", "B", 1),
          Path("A", "C", 2)
        ))
      }


      "work for small unweighted graph A -> B -> A" in {
        BreadthFirstSearchImpl(Digraph.fromEdges(Seq(
          RouteEdge("A", "B", 1),
          RouteEdge("B", "A", 1)
        ))).nearby("A") shouldBe Right(Seq(
          Path("A", "B", 1)
        ))
      }

      "work for small weighted graph A -3-> C, A -1-> B, B -1-> C" in {
        BreadthFirstSearchImpl(Digraph.fromEdges(Seq(
          RouteEdge("A", "C", 3),
          RouteEdge("A", "B", 1),
          RouteEdge("B", "C", 1)
        ))).nearby("A") shouldBe Right(Seq(
          Path("A", "B", 1),
          Path("A", "C", 2)
        ))
      }

      """
        | work for small weighted graph
        | CityA -3.45-> C, CityA -1.23-> CityB, CityB -1.14-> C,
        | parametrized by City objects and Double
      """.stripMargin in {
        trait City
        case object CityA extends City
        case object CityB extends City
        case object CityC extends City
        case class CityEdge(from: City,
                             to: City,
                             weight: Double) extends Edge[City, Double]

        implicit val tentativeMonoidInt = new TentativeMonoid[Double]
        implicit val tentativeOrderingInt = new TentativeOrdering[Double]

        BreadthFirstSearchImpl(Digraph.fromEdges(Seq(
          CityEdge(CityA, CityC, 3.45),
          CityEdge(CityA, CityB, 1.23),
          CityEdge(CityB, CityC, 1.14)
        ))).nearby(CityA) shouldBe Right(Seq(
          Path(CityA, CityB, 1.23),
          Path(CityA, CityC, 2.37)
        ))
      }

      val edgesNumbers = Seq(
        RouteEdge("5", "0", 1),
        RouteEdge("2", "4", 1),
        RouteEdge("3", "2", 1),
        RouteEdge("1", "2", 1),
        RouteEdge("0", "1", 1),
        RouteEdge("4", "3", 1),
        RouteEdge("3", "5", 1),
        RouteEdge("0", "2", 1)
      )

      "work for unweighted graph" in {
        val query = BreadthFirstSearchImpl(Digraph.fromEdges(edgesNumbers))
        query.nearby("0") shouldBe  Right(List(
          Path("0","1",1),
          Path("0","2",1),
          Path("0","4",2),
          Path("0","3",3),
          Path("0","5",4)))
      }


      "work for unweighted graph with max distance 3" in {
        val query = BreadthFirstSearchImpl(Digraph.fromEdges(edgesNumbers))
        query.nearby("0", Some(3)) shouldBe  Right(List(
          Path("0","1",1),
          Path("0","2",1),
          Path("0","4",2),
          Path("0","3",3)
        ))
        query.nearby("0", Some(1)) shouldBe  Right(List(
          Path("0","1",1),
          Path("0","2",1)
        ))
      }

      "work for example from task with max distance 130" in {
        val query = BreadthFirstSearchImpl(Digraph.fromEdges(exampleEdges))
        query.nearby("A", Some(130)) shouldBe  Right(List(
          Path("A","C",70),
          Path("A","D",120),
          Path("A","B",130)
        ))
      }
    }
  }
}
