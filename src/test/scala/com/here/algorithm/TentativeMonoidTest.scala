package com.here.algorithm

import cats.kernel.Monoid
import cats.implicits._
import com.here.graph._
import org.scalatest.{Matchers, WordSpec}

class TentativeMonoidTest extends WordSpec with Matchers {

  implicit val intAdditionMonoid: Monoid[TentativeDistance[Int]] = new TentativeMonoid[Int]()

  "TentativeMonoid" when {
    "needed to fill array values" should {
      "works properly" in {
        BreadthFirstSearch.fill[Int](5, 1) shouldBe Array(Infinity, Distance(0), Infinity, Infinity, Infinity)
      }
    }
  }
}
