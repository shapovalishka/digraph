package com.here.graph

import org.scalatest.{Matchers, WordSpec}

class DigraphFromEdgesTest extends WordSpec with Matchers {

  "A Digraph " when {

    "makeIndexation" should {
      "work with empty map" in {
        Digraph.makeIndexation[String](Seq("A", "B"), Map.empty, List.empty) shouldBe (Map("A" -> 0, "B" -> 1), List("A" -> 0, "B" -> 1))
        Digraph.makeIndexation[String](Seq(), Map.empty, List.empty) shouldBe (Map(), List())
      }
      "work with non empty map" in {
        Digraph.makeIndexation[String](Seq("A", "B"), Map("C" -> 0, "D" ->1), List.empty) shouldBe (
          Map("C" -> 0, "D" ->1, "A" -> 2, "B" -> 3),
          List("A" -> 2, "B" -> 3)
        )
      }
    }

    "addVertices" should {
      "add new only for new indexes for empty" in {
        Digraph.addVertices(List(0,1), Vector.empty[Vector[Int]]) shouldBe Vector(Vector(), Vector())
      }
      "add new only for new indexes for non empty" in {
        Digraph.addVertices(List(0,1), Vector(Vector(1), Vector(2))) shouldBe Vector(Vector(1), Vector(2))
        Digraph.addVertices(List(0,2), Vector(Vector(1), Vector(2))) shouldBe Vector(Vector(1), Vector(2), Vector())
        Digraph.addVertices(List(2,3), Vector(Vector(1), Vector(2))) shouldBe Vector(Vector(1), Vector(2), Vector(), Vector())
      }
    }

    "empty list of edges" should {
      "be empty as well" in {
        val d = Digraph.fromEdges(Seq.empty[RouteEdge])
        d.vertices.toList.map(_.toList) shouldBe List.empty[List[Int]]
        d.weights.toList.map(_.toList) shouldBe List.empty[List[Int]]
        d.indexes shouldBe Map.empty[String, Int]
      }
    }

    "another example" should {
      val edges = Seq(
        RouteEdge("5", "0", 1),
        RouteEdge("2", "4", 1),
        RouteEdge("3", "2", 1),
        RouteEdge("1", "2", 1),
        RouteEdge("0", "1", 1),
        RouteEdge("4", "3", 1),
        RouteEdge("3", "5", 1),
        RouteEdge("0", "2", 1)
      )
      "be constructed in right way" in {
        val d = Digraph.fromEdges(edges)
        d.vertices.toList.map(_.toList) shouldBe List(
          List(1),
          List(5,2),
          List(3),
          List(4),
          List(2,0),
          List(2)
        )
        d.indexes shouldBe Map(
          "5" -> 0,
          "0" -> 1,
          "2" -> 2,
          "4" -> 3,
          "3" -> 4,
          "1" -> 5
        )
      }
    }

    """[ A -> B: 240
      |  A -> C: 70
      |  A -> D: 120
      |  C -> B: 60
      |  D -> E: 480
      |  C -> E: 240
      |  B -> E: 210
      |  E -> A: 300] """ should {

      val edges = Seq(RouteEdge("A", "B", 240),
        RouteEdge("A", "C", 70),
        RouteEdge("A", "D", 120),
        RouteEdge("C", "B", 60),
        RouteEdge("D", "E", 480),
        RouteEdge("C", "E", 240),
        RouteEdge("B", "E", 210),
        RouteEdge("E", "A", 300))

      "be parsed in right way" in {
        val d = Digraph.fromEdges(edges)
        d.vertices.toList.map(_.toList) shouldBe List(
          List(1,2,3),
          List(4),
          List(1,4),
          List(4),
          List(0)
        )
        d.weights.toList.map(_.toList) shouldBe List(
          List(240,70,120),
          List(210),
          List(60,240),
          List(480),
          List(300)
        )
        d.indexes shouldBe Map(
          "A" -> 0,
          "B" -> 1,
          "C" -> 2,
          "D" -> 3,
          "E" -> 4
        )
      }

      "be parsed in right way [reversed]" in {
        val d = Digraph.fromEdges(edges.reverse)
        d.vertices.toList.map(_.toList) shouldBe List(
          List(1),
          List(4,3,2),
          List(0),
          List(0,2),
          List(0)
        )
        d.weights.toList.map(_.toList) shouldBe List(
          List(300),
          List(120,70,240),
          List(210),
          List(240, 60),
          List(480)
        )
        d.indexes shouldBe Map(
          "E" -> 0,
          "A" -> 1,
          "B" -> 2,
          "C" -> 3,
          "D" -> 4
        )
      }
    }
  }
}
