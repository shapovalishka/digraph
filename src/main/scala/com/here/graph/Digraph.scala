package com.here.graph

import scala.annotation.tailrec
import scala.collection.immutable.HashMap
import scala.reflect.ClassTag

/**
  * We store only were you can go, instead of full matrix.
  *
  * Instead of working with Vector[Vector[(Int, B)]],
  * I choosed two arrays of primitives( for neighbours
  * and weights) - saves a lot of memory.
  *
  * Let's assume we have 1024 vertices with 256 edges to neighbours each.
  * Vector of 1024 objects = 21,192
  * Vector of 256 objects = 5,448
  * Array of 1024 objects = 20,496
  * Array of 256 Int primitives = 1,040
  * size(Vector[Vector[(Int, Int)]]) ~ 21,192 + 1024 * 5,448 ~= 5 600 000
  * size(Array[Array[Int]]) ~ 20,496 + 1024 * 1,040 ~= 1 085 456
  *
  * Vector solution takes 2.5x more memory for storing whole graph.
  * P.S.: Benchmark data took from http://www.lihaoyi.com/post/BenchmarkingScalaCollections.html
  *
  * Also access to Array (constant time) by index is faster than Vector (effectively constant time)
  *
  * @param vertices
  * @param weights
  * @param indexes
  * @tparam A
  * @tparam B
  */
case class Digraph[A, B](vertices: Array[Array[Int]],
                         weights: Array[Array[B]],
                         indexes: Map[A, Int])

object Digraph {

  @tailrec
  def makeIndexation[A](names: Seq[A],
                        indexesMap: Map[A, Int],
                        indexes: List[(A, Int)]
                       ): (Map[A, Int], List[(A, Int)]) = {
    if (names.isEmpty) {
      (indexesMap, indexes.reverse)
    } else {
      val indexOpt = indexesMap.get(names.head)
      indexOpt match {
        case Some(index) =>
          makeIndexation(names.tail, indexesMap, (names.head -> index)::indexes)
        case None =>
          val newIndex = indexesMap.size
          val pair = names.head -> newIndex
          makeIndexation(names.tail, indexesMap + pair, pair::indexes)
      }
    }
  }

  /**
    * Add empty vector placeholders for the new Edges
    *
    * @param indexes
    * @param vertices
    * @return
    */
  def addVertices[T](indexes: List[Int], vertices: Vector[Vector[T]]): Vector[Vector[T]] =
    indexes.foldLeft[Vector[Vector[T]]](vertices)((vect: Vector[Vector[T]], elem: Int) =>
      if (elem > vect.length-1)
        vect.:+(Vector.empty[T])
      else
        vect
    )


  def fromEdges[A, B: ClassTag](edges: Seq[Edge[A, B]]): Digraph[A, B] = {

    /**
      * Append operations on vector should be more efficient than to an array
      * @param edges
      * @tparam A
      * @tparam B
      * @return
      */
    @tailrec
    def fromEdgesHelper[A, B](edges: Seq[Edge[A, B]],
                              vertices: Vector[Vector[(Int, B)]],
                              indexesMap: Map[A, Int]): (Vector[Vector[(Int, B)]], Map[A, Int]) = {
      if (edges.isEmpty)
        (vertices, indexesMap)
      else {
        val head= edges.head
        val (newIndexes, fromAndTo) =
          makeIndexation(Seq(head.from, head.to), indexesMap, List.empty)
        val fromIndexed :: toIndexed :: Nil = fromAndTo
        val newVertices = addVertices(fromAndTo.map(_._2), vertices)
        val withNewDirection = newVertices.apply(fromIndexed._2).:+(toIndexed._2 -> head.weight)
        fromEdgesHelper(edges.tail,
          newVertices.updated(fromIndexed._2, withNewDirection),
          newIndexes)
      }
    }

    val (verticesPairs, indexesMap) = fromEdgesHelper(edges, Vector.empty[Vector[(Int, B)]], HashMap.empty[A, Int])
    val arrayNeighbours = verticesPairs
    new Digraph[A, B](
      vertices = arrayNeighbours.map(_.map(_._1).toArray).toArray[Array[Int]],
      weights = arrayNeighbours.map(_.map(_._2).toArray[B]).toArray[Array[B]],
      indexes = indexesMap)
  }

}