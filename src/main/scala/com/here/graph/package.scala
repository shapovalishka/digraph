package com.here

import cats._

package object graph {

  trait Edge[A, B] {
    def from: A
    def to: A
    def weight: B
  }

  case class Path[A, B](from: A,
                        to: A,
                        weight: B) extends Edge[A, B]
  case class SimplePath[A](from: A,
                        to: A) extends Edge[A, Option[Nothing]] {
    override def weight = Option.empty[Nothing]
  }

  case class RouteEdge(from: String,
                       to: String,
                       weight: Int) extends Edge[String, Int]


  trait TentativeDistance[+T]
  case object Infinity extends TentativeDistance[Nothing]
  case class Distance[+T](value: T) extends TentativeDistance[T]

  class TentativeMonoid[T](implicit monoidB: Monoid[T]) extends Monoid[TentativeDistance[T]] {
    override def empty: TentativeDistance[T] = Infinity

    override def combine(x: TentativeDistance[T], y: TentativeDistance[T]): TentativeDistance[T] = (x, y) match {
      case (Distance(xx), Distance(yy)) => Distance(monoidB.combine(xx,yy))
      case (xx, Infinity) => xx
      case (Infinity, yy) => yy
    }
  }

  class TentativeOrdering[T](implicit ord: Ordering[T]) extends Ordering[TentativeDistance[T]] {
    override def compare(x: TentativeDistance[T], y: TentativeDistance[T]): Int = (x, y) match {
      case (Distance(xx), Distance(yy)) => ord.compare(xx,yy)
      case (Infinity, Infinity) => 0
      case (_, Infinity) => -1
      case (Infinity, _) => 1
    }
  }

}
