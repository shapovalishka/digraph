package com.here.algorithm

import cats.Monoid
import cats.implicits._
import com.here.graph._
import com.typesafe.scalalogging.LazyLogging

import scala.annotation.tailrec
import scala.collection.immutable.{IntMap, Queue}
import scala.reflect.ClassTag


object BreadthFirstSearch {
  trait SearchError
  case object VerticeIsNotInGraphError extends SearchError
  case object DestinationIsNotReachableError extends SearchError

  def fill[B: ClassTag](size: Int, startIndex: Int)(implicit monoidTent: Monoid[TentativeDistance[B]], monoidB: Monoid[B]) =
    Array.fill[TentativeDistance[B]](size){monoidTent.empty}.updated(startIndex, Distance(monoidB.empty))

}

trait BreadthFirstSearch[A, B] {
  import BreadthFirstSearch._
  def graph: Digraph[A, B]
  def route(from: A, to: A): Either[SearchError, (Seq[SimplePath[A]], B)]
  def nearby(from: A, maximumDistanceOpt: Option[B] = Option.empty): Either[SearchError, Seq[Edge[A, B]]]
}

case class BreadthFirstSearchImpl[A: ClassTag, B: ClassTag](graph: Digraph[A, B])
                                                           (implicit monoidB: Monoid[B],
                                                        ordB: Ordering[B],
                                                        ordTentB: Ordering[TentativeDistance[B]],
                                                        monoidTent: Monoid[TentativeDistance[B]]) extends BreadthFirstSearch[A, B] with LazyLogging {

  import BreadthFirstSearch._

  private def log(names: Map[Int,A], currentIndex: Int, neighbours: Array[Int], weights: Array[B]) = {
    logger.debug(
      s""" >> current index [${names(currentIndex)}] - ${currentIndex}"
         | n: ${neighbours.toList}
         | w: ${weights.toList}
       """.stripMargin)
  }


  @tailrec
  private def find(currentIndex: Int,
           toVisit: Queue[Int],
           edgeTo: Array[Int],
           distanceTo: Array[TentativeDistance[B]],
           stopEnqueue: (Int, Array[TentativeDistance[B]]) => Boolean,
           names: Map[Int,A] //only for debugging
          ): (Array[Int], Array[TentativeDistance[B]]) = {
    val currentDistance = distanceTo(currentIndex)
    val neighbours = graph.vertices(currentIndex)
    val weights = graph.weights(currentIndex)
    log(names, currentIndex, neighbours, weights)

    val queue =
      neighbours.zip(weights)
        .foldLeft(toVisit) { case (queue, (index, weightToIndex)) =>
          // distance to index is infinity - update
          // distance to index is bigger than (currentDistance + weightToIndex) - update
          val newDistance = monoidTent.combine(currentDistance, Distance(weightToIndex))
          logger.debug(s"check ([${names.apply(index)}] - $index, $weightToIndex)")
          val isVisited = Option(distanceTo(index)).filter(_ != monoidTent.empty)

          isVisited
            .filter(dist => {
              val res = ordTentB.lt(dist, newDistance)
              logger.debug(s"compare [$dist < $newDistance] = $res")
              res
            })
            .getOrElse {
              logger.debug(s"update ([${names.apply(index)}] - $index)")
              edgeTo.update(index, currentIndex)
              distanceTo.update(index, newDistance)
            }

          isVisited.map(_ => queue) getOrElse {
            logger.debug(s"put ([${names.apply(index)}] - $index)")
            if (stopEnqueue(index, distanceTo)) queue else queue enqueue index
          }
        }

    val nextStepOpt = queue.dequeueOption
    if (nextStepOpt.isDefined) {
      val (newIndex, newQueue) = nextStepOpt.get
      find(newIndex, newQueue, edgeTo, distanceTo, stopEnqueue, names)
    } else (edgeTo, distanceTo)
  }

  private def convertToPath(tentativeDistances: Array[TentativeDistance[B]],
                            from: A,
                            maximumOpt: Option[TentativeDistance[B]],
                            names: Map[Int, A]): Seq[Edge[A, B]] =
    tentativeDistances.zipWithIndex
      .filter(dist =>
          (dist._1 != monoidTent.empty) &&
          (dist._1 != Distance(monoidB.empty)) &&
          maximumOpt.forall(maximum => ordTentB.lteq(dist._1, maximum))
      )
      .foldLeft(List.empty[Edge[A, B]]) {case (accum, (Distance(weight), index)) =>
        Path (from = from, to = names.apply(index), weight = weight) :: accum
      }
      .sortBy(_.weight)

  def nearby(from: A, maximumDistanceOpt: Option[B] = Option.empty): Either[SearchError, Seq[Edge[A, B]]] = {
    val size = graph.vertices.length
    val names = IntMap.apply(graph.indexes.map( _.swap).toSeq: _*)

    logger.debug(s"NAMES : [$names]")

    Either.fromOption(graph.indexes.get(from), VerticeIsNotInGraphError).right.map { startIndex =>
      val stopF: (Int, Array[TentativeDistance[B]]) => Boolean = (indx, distances) =>
        maximumDistanceOpt.exists { max =>
          val distance = distances(indx)
          ordTentB.gteq(distance, Distance(max))
        }
      val (_, distances) = find(currentIndex = startIndex,
        Queue.empty[Int],
        edgeTo = new Array[Int](size),
        distanceTo = fill[B](size, startIndex),
        stopEnqueue = stopF,
        names = names
      )

      convertToPath(distances, from, maximumDistanceOpt.map(Distance(_)), names)
    }
  }

  @tailrec
  private def backwardsConvert(currentIndex: Int,
                startIndex: Int,
                edges: Array[Int],
                weights: Array[Array[B]],
                names: Map[Int, A],
                accum: List[SimplePath[A]]): List[SimplePath[A]] = {
    if (currentIndex == startIndex)
      accum
    else {
      val to = currentIndex
      val from = edges(to)
      backwardsConvert(from, startIndex, edges, weights, names, SimplePath(names.apply(from), names.apply(to))::accum)
    }
  }

  def route(from: A, to: A): Either[SearchError, (Seq[SimplePath[A]], B)] = {
    val size = graph.vertices.length
    val names = IntMap.apply(graph.indexes.map( _.swap).toSeq: _*)

    logger.debug(s"NAMES : [$names]")
    val indexesOpt = for {
      start <- graph.indexes.get(from)
      end <- graph.indexes.get(to)
    } yield (start, end)

    Either.fromOption(indexesOpt, VerticeIsNotInGraphError).right.flatMap { case (startIndex, endIndex) =>
      val (edges, distances) = find(currentIndex = startIndex,
        Queue.empty[Int],
        edgeTo = new Array[Int](size),
        distanceTo = fill[B](size, startIndex),
        stopEnqueue = (index, _) => index == endIndex,
        names = names
      )
      val res = ordTentB.equiv(distances(endIndex), monoidTent.empty)
      logger.debug(s"${distances(endIndex)} - ${monoidTent.empty} = [$res]")
      if (res)
        Either.left(DestinationIsNotReachableError)
      else {
        val Distance(distance) = distances(endIndex)
        Either.right[SearchError, (Seq[SimplePath[A]], B)](
          (backwardsConvert(endIndex, startIndex, edges, graph.weights, names, List.empty), distance)
        )
      }
    }
  }
}
