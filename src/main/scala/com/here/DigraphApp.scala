package com.here

import java.io.{BufferedReader, File, FileInputStream, InputStreamReader}

import cats.Traverse
import com.here.algorithm.BreadthFirstSearchImpl
import com.here.graph._
import cats.implicits._

import scala.util.Try

object DigraphApp extends App {
  import com.here.algorithm.BreadthFirstSearch._


  implicit val tentativeMonoidInt = new TentativeMonoid[Int]
  implicit val tentativeOrderingInt = new TentativeOrdering[Int]

  val path = Try(args(0)).toOption.filter(new File(_).exists()).getOrElse {
    println(
      """
        |Failed to read input file path from command-line arguments or such file doesn't exist.
        |Please run:
        |
        |sbt "run-main com.here.DigraphApp /path/to/the/file"
        |
        |Here is an example result:
      """.stripMargin)
    getClass
      .getClassLoader
      .getResource("example-data.txt")
      .getPath
  }

  def parseEdges(line: String): Try[RouteEdge] = Try {
    val left::right::Nil = line.split(':').toList
    val from::to::Nil = left.split("->").toList
    RouteEdge(
      from = from.trim,
      to = to.trim,
      weight = right.trim.toInt
    )
  }

  def parseSimplePath(line: String): Try[SimplePath[String]] = Try {
    val left::right::Nil = line.split("route").toList
    val from::to::Nil = right.split("->").toList
    SimplePath(
      from = from.trim,
      to = to.trim
    )
  }

  def parseNearby(line: String): Try[(String, Int)] = Try {
    val left::right::Nil = line.split("nearby").toList
    val from::max::Nil = right.split(",").toList
    (from.trim, max.trim.toInt)
  }


  def handleError(pathOpt: Option[SimplePath[String]]): PartialFunction[SearchError, Unit] = {
    case DestinationIsNotReachableError if pathOpt.isDefined =>
      pathOpt.foreach { path =>
        println(s"Error: No route from <${path.from}> to <${path.to}>")
      }
    case VerticeIsNotInGraphError => println(s"Error: Wrong input data for query")
    case _ => println(s"Error: Unknown error")
  }


  val fIn = new FileInputStream(path)
  val in = new BufferedReader(new InputStreamReader(fIn))
  val edgesOpt = Try(in readLine()).map(_.toInt).toOption.flatMap { numOfLines =>
    Traverse[List].sequence((1 to numOfLines).toList.map(_ => Try(in readLine()).flatMap(parseEdges).toOption))
  }
  val simplePathOpt = Try(in readLine()).flatMap(parseSimplePath).toOption
  val nearByOpt = Try(in readLine()).flatMap(parseNearby).toOption
  (for {
    edges <- edgesOpt
    path <- simplePathOpt
    (from, distance) <- nearByOpt
    query = BreadthFirstSearchImpl(Digraph.fromEdges(edges = edges))
  } yield {
    query.route(path.from, path.to).fold(handleError(Some(path)),{ case (simplePaths, length) =>
      println(simplePaths.tail.foldLeft(s"${simplePaths.head.from}")((accum, path) => s"$accum -> ${path.to}") + s" : $length")
    })
    query.nearby(from, Some(distance)).fold(handleError(None),{ paths =>
      println(paths.tail.foldLeft(s"${paths.head.to} : ${paths.head.weight}")( (accum, path) => s"$accum, ${path.to} : ${path.weight}"))
    })
  }).getOrElse {
    println(s"Error: Failed to parse>")
  }
}
