name := "digraphs"

version := "1.0"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq("com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "org.typelevel" %% "cats-core" % "1.0.0-MF",
  "org.scalatest" %% "scalatest" % "3.0.4" % "test")
